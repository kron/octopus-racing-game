from pygame import mixer
import numpy as np
import cv2
from datetime import datetime
import pygame
from enum import Enum
from dataclasses import dataclass
from utils import distanceCalculate
from ball_detection import BallDetection

class Colors(Enum):
    BLUE =  [100, 0, 0]
    YELLOW = [0, 255, 255]

DEFAULT_BACKGROUND = pygame.image.load("./assets/background.gif")
DISPLAY_WIDTH = 1300
DISPLAY_HEIGHT = 1300

octopus_image = pygame.image.load('./assets/polpo.png')

class Game():
	name = "Name of the game"
	description = "Description of the game"
	author = "Your name"
	
	def process_frame(self, frame, display):
		pass

@dataclass
class PlayerPoints:
	points: int
	player: object

class SimpleRace(Game):
	def __init__(self):
		self.started_at = datetime.now()
		self.players = []
		self.ball_detection = BallDetection(colors=[Colors.BLUE.value, Colors.YELLOW.value])

		# self.add_player(name="yellow", color=YELLOW, sound_file="./assets/audio.mp3")
		# self.add_player(name="blue", color=BLUE, sound_file="./assets/audio.mp3")

	"""def add_player(self, *args, **kwargs):
		player = Player(*args, **kwargs)
		self.players.append(PlayerPoints(player=player, points=0))
		return player"""

	def process_frame(self, frame, display): 
		display.blit(DEFAULT_BACKGROUND, (0, 0))
		found, detected_ball = self.ball_detection.find_ball_in_frame(frame)
		if found:
			print(detected_ball)
		
		
		# for player in self.players:
		#		player.player.process_frame(frame)
		
		# display.blit(carImg, (DISPLAY_HEIGHT/4 , (DISPLAY_HEIGHT * 0.8) - (DISPLAY_HEIGHT * 0.1) * self.players[0].points))
		# display.blit(carImg, (DISPLAY_HEIGHT-(DISPLAY_HEIGHT/4)  , (DISPLAY_HEIGHT * 0.8) - (DISPLAY_HEIGHT * 0.1) * self.players[0].points))
 
"""
class Player:
	def update_points(self):
		if (self.is_visible == False):
			last_point_delta = datetime.now() - self.last_point               # For build-in functions
			last_point_delta_second = last_point_delta.total_seconds()      # Total number of seconds between dates
			if (last_point_delta_second > 1):
				self.last_point = datetime.now()
				self.points += 1
				self.sound.play()
				print(self.name,self.points)
"""