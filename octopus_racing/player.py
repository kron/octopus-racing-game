from enum import Enum

class Colors(Enum):
    BLUE =  [255, 0, 0]
    YELLOW = [0, 255, 255]

@dataclass
class Chatacter:
    display_name: str
    color: Colors

class Characters(Enum):
    FABLAB = Character(display_name="Il polpo del fablab", color=Colors.BLUE)
    CIRCO = Character(display_name="Il polpo del circo", color=Colors.YELLOW)

    def __str__(self):
        return self.value.display_name
