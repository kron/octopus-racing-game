import numpy as np
import cv2
from dataclasses import dataclass

@dataclass
class FoundedBall:
	color: list
	center: tuple 
	radius: int

class BallDetection():
	def __init__(self, colors):
		self.color = None
		self.colors = colors
		self.colors_cache = {}

	def get_hsv_range(self, bgr_color): 
		"""if self.color in self.colors_cache:
			color_lower = self.colors_cache[bgr_color]["color_lower"]
			color_upper = self.colors_cache[bgr_color]["color_upper"]
		else:"""
		color = np.uint8([[bgr_color]])
		hsv_color = cv2.cvtColor(color, cv2.COLOR_BGR2HSV)
		hue = hsv_color[0][0][0]
		color_lower = (int(hue-10), 100, 100)
		color_upper = (int(hue+10), 255, 255)
		# self.colors_cache["bgr_color"] = {
		#	"color_lower": color_lower, 
		#	"color_upper": color_upper}
		return color_lower, color_upper

	def convert_frame_color(self, frame):
		# blur it and convert it to the HSV color space
		blurred = cv2.GaussianBlur(frame, (11, 11), 0)
		hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)
		return hsv

	def create_frame_mask(self, hsv_frame, bgr_color):
		color_lower, color_upper = self.get_hsv_range(bgr_color)
		mask = cv2.inRange(hsv_frame, color_lower, color_upper)
		mask = cv2.erode(mask, None, iterations=2)
		mask = cv2.dilate(mask, None, iterations=2)
		return mask

	def find_ball_in_frame(self, frame, color=None):
		founded_balls = []
		for color in self.colors:
			hsv_frame = self.convert_frame_color(frame)
			mask = self.create_frame_mask(hsv_frame, color)
			cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
			cnts = cnts[0]
			if len(cnts) > 0:
				# find the largest contour in the mask, then use
				# it to compute the minimum enclosing circle and
				# centroid
				c = max(cnts, key=cv2.contourArea)
				((x, y), radius) = cv2.minEnclosingCircle(c)
				print("R", radius)
				if (radius >= 10):
					M = cv2.moments(c)
					center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
					#self.self.colors_cache[bgr_color]["is_visible"] = True
					founded_ball = FoundedBall(color, center, radius)
					founded_balls.append(founded_ball)
					cv2.circle(frame, (int(x), int(y)), int(radius),(0, 255, 255), 2)
					cv2.circle(frame, center, 5, (0, 0, 255), -1)

					# return True, color #{"color": color, "center": center, "radius": radius}
			# self.self.colors_cache[bgr_color]["is_visible"] = False
	
		found = len(founded_balls) > 0
		return found, founded_balls