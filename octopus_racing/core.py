import pygame
import cv2
from game import Game, SimpleRace

DISPLAY_WIDTH = 1300
DISPLAY_HEIGHT = 1300
DEFAULT_BACKGROUND = pygame.image.load("./assets/background.gif")
CAMERA_NUMBER = 2
DEBUG = True

class Machine():
    def __init__(self):
        self.camera = cv2.VideoCapture(CAMERA_NUMBER)
        self.camera_width  = self.camera.get(cv2.CAP_PROP_FRAME_WIDTH)   # float `width`
        self.camera_height = self.camera.get(cv2.CAP_PROP_FRAME_HEIGHT)  # float `height`

        pygame.init()
        self.display = pygame.display.set_mode((DISPLAY_WIDTH,DISPLAY_HEIGHT))
        self.game = SimpleRace()

    def loop(self):
        while(True):            
            ret, frame = self.camera.read()
            frame = cv2.flip(frame, 1)
            self.game.process_frame(frame, self.display)
            pygame.display.update()

            if DEBUG:
                center_of_frame = (int(self.camera_width/2), int(self.camera_height/2))
                cv2.circle(frame, center_of_frame, 3, (0, 255, 0), -1)
                cv2.circle(frame, center_of_frame, 75, (0, 255, 0), 2)
                cv2.circle(frame, center_of_frame, 150, (0, 255, 0), 2)
                cv2.circle(frame, center_of_frame, 225, (0, 255, 0), 2)
                cv2.imshow("Frame", frame)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                self.close()
                break

    def close(self):
        self.camera.release()
        cv2.destroyAllWindows()

machine = Machine()
machine.loop() 
