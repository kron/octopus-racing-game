"""
@dataclass
class Character:
	display_name: str
	image_file: str = "./assets/polpo.png"
	sound_file: str = "./assets/audio.mp3"
	color: object

class Characters(Enum):
	FABLAB = Character(display_name="Il polpo del fablab", color=Colors.BLUE.value)
	CIRCO = Character(display_name="Il polpo del circo", color=Colors.YELLOW.value)

	def __str__(self):
		return self.value.display_name
"""
