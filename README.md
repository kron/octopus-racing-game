# Octopus Racing Game

Software part of the octopus racing game, inspired by the more classic fairground game Horse Racing and reinvented with modern software technology.

Opencv is used to identify the balls and pygame is used to manage the GUI.
